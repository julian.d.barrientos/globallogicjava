package com.barrientos.meli.Activities

import com.barrientos.meli.R
import android.app.Activity
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.widget.Toast
import com.barrientos.meli.Models.MapClass
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mercadolibre.android.sdk.ApiResponse
import com.mercadolibre.android.sdk.Meli


class SplashActivity: Activity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Meli.initializeSDK(applicationContext)
        SplashTask(this).execute(object : Command() {
            override fun executeCommand(): ApiResponse? {

                return  Meli.get(getString(R.string.categoriesLink))
            }
        })

    }


    private class SplashTask(private var activity: Activity) : AsyncTask<Command, Void, ApiResponse>() {

        override fun doInBackground(vararg params: Command): ApiResponse? {
            return params[0].executeCommand()
        }

        override fun onPostExecute(apiResponse: ApiResponse?) {
            super.onPostExecute(apiResponse)
            if(apiResponse == null){
                Toast.makeText(activity.applicationContext, "MAL", Toast.LENGTH_SHORT).show()
            }else{

                val response : Array<MapClass> = Gson().fromJson(apiResponse.content, object : TypeToken<Array<MapClass>>() {}.type)

                val intet: Intent = Intent(activity.applicationContext,MainActivity::class.java).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intet.putExtra(activity.getString(R.string.categoriesExtra),response)
                activity.applicationContext.startActivity(intet)
                activity.finish()
            }
        }
    }
    private abstract class Command {
        abstract fun executeCommand(): ApiResponse?
    }
}