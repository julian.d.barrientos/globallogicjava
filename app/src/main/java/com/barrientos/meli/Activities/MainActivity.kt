package com.barrientos.meli.Activities

import android.app.SearchManager
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.provider.SearchRecentSuggestions
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.barrientos.meli.Utils.ItemsAdapter
import com.barrientos.meli.Models.MapClass
import com.barrientos.meli.Models.Response
import com.barrientos.meli.R
import com.barrientos.meli.Pager.SectionsPagerAdapter
import com.barrientos.meli.Utils.SuggestProvider
import com.google.android.material.tabs.TabLayout
import com.google.gson.Gson
import com.mercadolibre.android.sdk.ApiResponse
import com.mercadolibre.android.sdk.Meli


class MainActivity : AppCompatActivity() {

    private lateinit var categories: Array<MapClass>
    private lateinit var viewPager: ViewPager
    private lateinit var buttonSearch: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        categories = intent.getSerializableExtra(getString(R.string.categoriesExtra)) as Array<MapClass>
        //set the tabs and the pager
        setPager()
        //set ui components
        setUI()
        // Initialize Mercado Libre SDK
        Meli.initializeSDK(applicationContext)
        handleSearch(intent)
    }




    private fun setPager(){
        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager,categories)
        viewPager = findViewById(R.id.view_pager)

        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
    }
    private fun setUI(){
        this.buttonSearch = findViewById(R.id.search_button)

        buttonSearch.setOnClickListener(View.OnClickListener(){
            onSearchRequested()
        })
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleSearch(intent)
    }

    private fun handleSearch(intent: Intent) {
        if (Intent.ACTION_SEARCH == intent.action) {
            intent.getStringExtra(SearchManager.QUERY)?.also { query ->
                SearchRecentSuggestions(this, SuggestProvider.AUTHORITY, SuggestProvider.MODE)
                    .saveRecentQuery(query, null)
                startActivity(Intent(this,SearchActivity::class.java).putExtra("search",query))
            }
        }
    }


}
