package com.barrientos.meli.Activities

import android.os.AsyncTask
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.Utils.ItemsAdapter
import com.barrientos.meli.Models.Response
import com.barrientos.meli.R
import com.google.gson.Gson
import com.mercadolibre.android.sdk.ApiResponse
import com.mercadolibre.android.sdk.Meli


class SearchActivity: AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        val toolbar: Toolbar? = findViewById(R.id.search_appbar)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        val search:String? = intent.getStringExtra("search")

        Meli.initializeSDK(applicationContext)

        viewManager = LinearLayoutManager(this)

        ItemsTask(this).execute(object : Command() {
            override fun executeCommand(): ApiResponse? {

                return  Meli.get("/sites/MLA/search?q=$search")
            }
        })
    }

    private class ItemsTask(private var activity: SearchActivity) : AsyncTask<Command, Void, ApiResponse>() {

        override fun onPreExecute() {

        }
        override fun onPostExecute(apiResponse: ApiResponse?) {
            super.onPostExecute(apiResponse)
            activity.findViewById<ProgressBar>(R.id.pg_loading).visibility = View.GONE

            activity.findViewById<RecyclerView>(R.id.ReView_items).visibility = View.VISIBLE
            if (apiResponse == null) {
                Toast.makeText(activity.applicationContext, "MAL", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(activity.applicationContext, "BIEN", Toast.LENGTH_SHORT).show()


                val response : Response = Gson().fromJson(apiResponse.content, Response::class.java)

                activity.viewAdapter =
                    ItemsAdapter(response.results)

                activity.recyclerView = activity.findViewById<RecyclerView>(R.id.ReView_items).apply {
                    // use this setting to improve performance if you know that changes
                    // in content do not change the layout size of the RecyclerView
                    setHasFixedSize(true)

                    // use a linear layout manager
                    layoutManager = activity.viewManager

                    // specify an viewAdapter (see also next example)
                    adapter = activity.viewAdapter

                }
            }
        }
        override fun doInBackground(vararg params: Command): ApiResponse? {
            return params[0].executeCommand()
        }
    }

    private abstract class Command {
        abstract fun executeCommand(): ApiResponse?
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}