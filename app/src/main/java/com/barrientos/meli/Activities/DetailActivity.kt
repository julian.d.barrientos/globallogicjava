package com.barrientos.meli.Activities


import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.Models.Item
import com.barrientos.meli.R
import java.text.NumberFormat
import java.util.*
import androidx.core.app.NavUtils
import android.view.MenuItem


class DetailActivity: AppCompatActivity() {

    private lateinit var  itemTitle: TextView
    private lateinit var  itemVendor: TextView
    private lateinit var  itemPrice: TextView
    private lateinit var  itemPriceDecimal: TextView
    private lateinit var  itemState: TextView
    private lateinit var  item: Item
    private lateinit var  format: NumberFormat


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        item = intent.getSerializableExtra("item") as Item

        format = NumberFormat.getCurrencyInstance(Locale("es","AR"))

        setUI()
        setTitleItem()
    }

    private fun setUI(){
        this.itemTitle = findViewById(R.id.item_title)
        this.itemVendor = findViewById(R.id.item_vendor)
        this.itemPrice = findViewById(R.id.item_price)
        this.itemPriceDecimal = findViewById(R.id.item_price_decimal)
        this.itemState = findViewById(R.id.item_state)
    }

    private fun setTitleItem(){
        this.itemTitle.text = this.item.title

        val formatted = format.format(item.price).split(",")

        this.itemPrice.text = formatted[0]
        this.itemPriceDecimal.text = formatted[1]

        this.itemState.text =  getString(R.string.condition,item.condition,item.sold_quantity)
    }
    private fun setDetail(){

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
