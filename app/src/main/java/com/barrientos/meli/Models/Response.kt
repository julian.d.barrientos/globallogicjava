package com.barrientos.meli.Models

import java.io.Serializable

class Response () :Serializable{
    var siteId : String = ""
    var paging : Paging = Paging()
    var results : List<Item> = listOf()
    var secondaryResults : List<Item> = listOf()
    var relatedResults : List<Item> = listOf()
    var sort: MapClass = MapClass()
    var availableSorts: List<MapClass> = listOf()
    var filters: List<Filter> = listOf()
    var availableFilters: List<AvailableFilter> = listOf()

    constructor(siteId : String, paging : Paging, results : List<Item>, secondaryResults : List<Item>, relatedResults : List<Item>, sort: MapClass, availableSorts: List<MapClass>,
                filters: List<Filter>,availableFilters: List<AvailableFilter>):this(){
        this.siteId = siteId
        this.paging = paging
        this.results = results
        this.secondaryResults = secondaryResults
        this.relatedResults = relatedResults
        this.sort = sort
        this.availableSorts = availableSorts
        this.filters = filters
        this.availableFilters = availableFilters
    }
    inner class Paging () :Serializable {
        var total : Number = 0
        var offset : Number = 0
        var limit : Number = 0
        var primary_results : Number = 0

        constructor(total : Number, offset : Number, limit : Number, primary_results : Number) : this(){
            this.total = total
            this.offset = offset
            this.limit = limit
            this.primary_results = primary_results
        }
    }



    inner class Filter () :Serializable {
        var id: String = ""
        var name: String = ""
        var type: String = ""
        var values: List<ValueFilters> = listOf()

        constructor(id: String, name: String, type: String, values: List<ValueFilters>):this(){
            this.id = id
            this.name = name
            this.type = type
            this.values = values
        }
    }

    inner class ValueFilters(id: String, name: String, var pathFromRoot: List<MapClass> = listOf())   :Serializable, MapClass(id,name)

    inner class AvailableFilter () :Serializable {
        var id: String = ""
        var name: String = ""
        var type: String = ""
        var values: List<ValueAvailableFilter> = listOf()

        constructor(id: String, name: String, type: String, values: List<ValueAvailableFilter>):this(){
            this.id = id
            this.name = name
            this.type = type
            this.values = values
        }
    }

    inner class ValueAvailableFilter () :Serializable {
        var id: String = ""
        var name: String = ""
        var results: Long = 0

        constructor(id: String, name: String, results: Long):this(){
            this.id = id
            this.name = name
            this.results = results
        }
    }
}


