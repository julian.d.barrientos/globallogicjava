package com.barrientos.meli.Models

import java.io.Serializable

open class MapClass ():Serializable{
    var id:String = ""
    var name:String = ""

    constructor(id: String, name:String):this(){
        this.id = id
        this.name = name
    }
}