package com.barrientos.meli.Utils

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.Activities.DetailActivity
import com.barrientos.meli.Models.Item
import com.barrientos.meli.R
import com.bumptech.glide.Glide
import java.text.NumberFormat
import java.util.*

class ItemsAdapter(private var items: List<Item>) :
    RecyclerView.Adapter<ItemsAdapter.ItemsHolder>() {


    var format: NumberFormat = NumberFormat.getCurrencyInstance(Locale("es","AR"))

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    inner class ItemsHolder(val view: View) : RecyclerView.ViewHolder(view){

        fun bindItems(item: Item){
            val itemTitle : TextView = itemView.findViewById(R.id.card_item_title)
            val image : ImageView = itemView.findViewById(R.id.card_item_image)
            val currency: TextView = itemView.findViewById(R.id.card_item_price)
            val currencyDecimal: TextView = itemView.findViewById(R.id.card_item_decimal)

            val formatted = format.format(item.price).split(",")
            itemTitle.text = item.title
            currency.text = formatted[0]
            currencyDecimal.text = formatted[1]

            Glide.with(itemView).load(item.thumbnail).into(image)
            itemView.setOnClickListener{
                val contxt: Context = it.context
                val intet: Intent = Intent(contxt,DetailActivity::class.java)
                intet.putExtra("item",item)
                contxt.startActivity(intet)
            }
        }
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ItemsHolder {
        // create a new view

        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_list,parent,false)


        // set the view's size, margins, paddings and layout parameters

        return ItemsHolder(v)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ItemsHolder, position: Int) {

        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val item = items[position]
        holder.bindItems(item)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = items.size

    fun setData(newItems: List<Item>){
        this.items = newItems
    }
}