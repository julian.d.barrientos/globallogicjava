package com.barrientos.meli.Utils


import android.os.AsyncTask
import com.barrientos.meli.Interface.BasicCommand
import com.barrientos.meli.Interface.GetItemsCommand
import com.barrientos.meli.Models.Response
import com.google.gson.Gson
import com.mercadolibre.android.sdk.ApiResponse

class ItemsTask(private var showResults: BasicCommand) : AsyncTask<GetItemsCommand, Void, ApiResponse>() {

    override fun onPostExecute(apiResponse: ApiResponse?) {
        super.onPostExecute(apiResponse)

        if (apiResponse == null) {
           // Toast.makeText(activity.applicationContext, "MAL", Toast.LENGTH_SHORT).show()
        } else {
           // Toast.makeText(activity.applicationContext, "BIEN", Toast.LENGTH_SHORT).show()
            val response : Response = Gson().fromJson(apiResponse.content, Response::class.java)
            showResults.executeCommand(response.results)
        }
    }
    override fun doInBackground(vararg params: GetItemsCommand): ApiResponse? {
        return params[0].executeCommand()
    }
}