package com.barrientos.meli.Utils

import android.content.SearchRecentSuggestionsProvider

class SuggestProvider : SearchRecentSuggestionsProvider() {
    init {
        setupSuggestions(
            AUTHORITY,
            MODE
        )
    }

    companion object {
        const val AUTHORITY = "com.barrientos.meli.Utils.SuggestProvider"
        const val MODE: Int = SearchRecentSuggestionsProvider.DATABASE_MODE_QUERIES
    }
}