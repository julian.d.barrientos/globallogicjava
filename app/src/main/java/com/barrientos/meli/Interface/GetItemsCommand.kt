package com.barrientos.meli.Interface

import com.mercadolibre.android.sdk.ApiResponse

interface GetItemsCommand {
    fun executeCommand(): ApiResponse?
}