package com.barrientos.meli.Interface

import com.barrientos.meli.Models.Item

interface BasicCommand{
    fun executeCommand(response: List<Item>)
}