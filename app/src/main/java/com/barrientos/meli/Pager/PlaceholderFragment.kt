package com.barrientos.meli.Pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.barrientos.meli.Interface.BasicCommand
import com.barrientos.meli.Interface.GetItemsCommand
import com.barrientos.meli.Models.Item
import com.barrientos.meli.R
import com.barrientos.meli.Utils.ItemsAdapter
import com.barrientos.meli.Utils.ItemsTask
import com.mercadolibre.android.sdk.ApiResponse
import com.mercadolibre.android.sdk.Meli

/**
 * A placeholder fragment containing a simple view.
 */


class PlaceholderFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var mAdapter: ItemsAdapter

    private var items: List<Item> = arrayListOf()
    private lateinit var loading: ProgressBar
    private  var categoryId: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryId = arguments?.getString(ARG_SECTION_NUMBER)

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_main, container, false)

        loading = root.findViewById(R.id.pg_loading)
        recyclerView = root.findViewById(R.id.ReView_items)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(root.context)
        mAdapter = ItemsAdapter(items)
        recyclerView.adapter = mAdapter

        ItemsTask(object : BasicCommand {
            override fun executeCommand(response: List<Item>) {
                mAdapter.setData(response)
                loading.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                mAdapter.notifyDataSetChanged()
            }
        }).execute(object : GetItemsCommand {
            override fun executeCommand(): ApiResponse? {

                return Meli.get("/sites/MLA/search?category=$categoryId")
            }
        })
        return root
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(categoryId: String): PlaceholderFragment {
            return PlaceholderFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_SECTION_NUMBER, categoryId)
                }
            }
        }
    }
}