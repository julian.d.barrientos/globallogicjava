package com.barrientos.meli.Pager

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.barrientos.meli.Models.MapClass
import com.barrientos.meli.R

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager, private var categories: Array<MapClass> = arrayOf()) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        return PlaceholderFragment.newInstance(categories[position].id)
    }

    override fun getPageTitle(position: Int): CharSequence? {

        return categories[position].name
    }

    override fun getCount(): Int {
        // Show 2 total pages.
        return categories.size
    }
}